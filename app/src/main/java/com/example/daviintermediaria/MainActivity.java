package com.example.daviintermediaria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView Comprar;
    private ImageView Alugar;
    private ImageView Automacao;
    private ImageView Contatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("MHS Contrutora e Automação");
        Comprar = findViewById(R.id.idComprar);
        Alugar = findViewById(R.id.idAlugar);
        Contatos = findViewById(R.id.idContatos);
        Automacao = findViewById(R.id.idAutomacao);

        Comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, comprar.class));
            }
        });

        Alugar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent( MainActivity.this, alugar.class));
            }
        });

        Contatos.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                startActivity(new Intent( MainActivity.this, contatos.class));
            }
        } );

        Automacao.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                startActivity(new Intent( MainActivity.this, automacao.class));
            }
        });
    }
}
